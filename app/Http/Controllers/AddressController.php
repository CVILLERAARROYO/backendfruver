<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Address;

class AddressController extends Controller
{
    public function index()
    {
        return Inertia::render('Profile/address',[
            'Addresses' => Address::where('user_id', Auth()->user()->id)->get()
        ]);
    }
    public function store(Request $request){
        // $request->validate([
        //     "country" => "required",
        //     "departament" => "required",
        //     "city" => "required",
        //     "address" => "required",
        //     "reference" => "required",
        // ]);
        
        Address::create([
            "country" => $request->country,
            "code_country" => '001',
            "department" => $request->department,
            "code_department" => '01',
            "city" => $request->city,
            "code_city" => '009',
            "address" => $request->address,
            "reference" => $request->reference,
            "user_id" => Auth()->user()->id,
        ]);
        return redirect()->back()->with(['toast' => ['message' => 'Dirección creada']]);

    }
    public function update(Request $request){
        // $request->validate([
        //     "country" => "required",
        //     "departament" => "required",
        //     "city" => "required",
        //     "address" => "required",
        //     "reference" => "required",
        // ]);
        Address::where('id', $request->id)->update([
            "country" => $request->country,
            "code_country" => '001',
            "department" => $request->department,
            "code_department" => '01',
            "city" => $request->city,
            "code_city" => '009',
            "address" => $request->address,
            "reference" => $request->reference,
            "user_id" => Auth()->user()->id,
        ]);

        return redirect()->back()->with(['toast' => ['message' => 'Dirección actualizada']]);

    }
    public function destroy($id){
        Address::destroy($id);
        return redirect()->back()->with(['toast' => ['message' => 'Dirección eliminada']]);

    }
}
