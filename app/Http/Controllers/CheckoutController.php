<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;


class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()){
            return Inertia::render('Checkout/Index', [
                'Cart' => \Cart::getContent(),
                'Addresses' => Auth::user()->addresses
            ]);
        }else{
            return Inertia::render('Checkout/Index', [
                'Cart' => \Cart::getContent(),
            ]);
        }
      
    }
}
