<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/Category/Index', [
            'Categories' => Category::withCount('products')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('icon')) {
            $path_icon = $request->file('icon')->store('public');
            $path_icon = str_replace('public', 'storage', $path_icon);
        }
        if ($request->hasFile('bannerMovil')) {
            $path_url_banner = $request->file('bannerMovil')->store('public');
            $path_url_banner = str_replace('public', 'storage', $path_url_banner);
        }

        if ($request->hasFile('bannerDesktop')) {
            $path_banner_desktop = $request->file('bannerDesktop')->store('public');
            $path_banner_desktop = str_replace('public', 'storage', $path_banner_desktop);
        }
            Category::create([
                'url_icon' => isset($path_icon) ? $path_icon : null,
                'url_banner'  =>  isset($path_url_banner) ? $path_url_banner : null,
                'url_banner_desktop' => isset($path_banner_desktop) ? $path_banner_desktop : null,
                'name' => $request->name,
                'color' => $request->color,
                'description' => $request->description,
                'parent_id' => $request->parentCategory,
            ]);
        return redirect()->route('admin.product.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Category::destroy($id);
        return redirect()->back();
    }
}
