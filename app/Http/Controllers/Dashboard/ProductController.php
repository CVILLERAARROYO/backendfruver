<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Attribute as ModelsAttribute;
use App\Models\Category;
use App\Models\Meta;
use App\Models\Picture;
use App\Models\Product;
use App\Models\Value;
use Attribute;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('values.term')
            ->with('pictures')
            ->with('meta')
            ->with('category:id,name')
            ->orderBy('id', 'DESC')
            ->get();
        return Inertia::render(
            'Dashboard/Product/Index',
            [
                'Products' => $products
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Dashboard/Product/Create', [
            'Categories' => Category::select('id', 'name')
                ->where('id', '!=', 1)->get(),
            'Attributes' => ModelsAttribute::with('terms:id,name,attribute_id')->select('id', 'name')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $request->validate([
            'typeProduct' => 'required',
        ]);
        if ($request->typeProduct == 'simple') {
            $request->validate([
                'productTypeCustomer' => 'required',
                'name' => 'required',
                'description' => 'required',
                'priceSimple' => 'required',
                'unitName' => 'required',
                'valueUnit' => 'required',
            ]);
        } else if ($request->typeProduct == 'variable') {
            $request->validate([
                'productTypeCustomer' => 'required',
                'name' => 'required',
                'description' => 'required',
                'terms' => 'required',
            ]);
        }
        if ($request->hasFile('picture')) {
            $path_picture = $request->file('picture')->store('public');
            $path_picture = str_replace('public', 'storage', $path_picture);
        }
        if ($request->typeProduct == 'simple') {
            $productTmp = Product::create([
                'name' => $request->name,
                'category_id' => $request->category != null ? $request->category : 1,
                'product_type' => $request->typeProduct,
                'sku' => $request->sku,
                'product_status' => $request->statusInventory,
                'product_type_customer' => $request->productTypeCustomer,
            ])->meta()->create([
                'origin' => $request->origin,
                'price' => $request->priceSimple,
                'stock' => $request->stock != null ? $request->stock : 0,
                'unit' => $request->unitName,
                'value_unit' => $request->valueUnit,
                'few_units' => 5,
                'description' => $request->description,
            ]);
            if ($request->hasFile('picture') && $path_picture) {
                Picture::create([
                    'product_id' =>  $productTmp->id,
                    'url' =>  $path_picture,
                ]);
            }

            return redirect()->route('admin.product.index');
        } else  if ($request->typeProduct == 'variable') {
            $productTmp = Product::create([
                'name' => $request->name,
                'category_id' => $request->category != null ? $request->category : 1,
                'product_type' => $request->typeProduct,
                'product_status' => $request->statusInventory,
                'product_type_customer' => $request->productTypeCustomer,
            ]);
            foreach ($request->terms as $key => $term) {
                if (isset($term['show'])) {
                    Value::create([
                        'price' => $term['price'],
                        'sku' =>  $term['sku'],
                        'product_status' => 'existencia',
                        'stock' =>  rand(1, 100),
                        'few_units' =>   $term['few_units'],
                        'term_id' =>  $term['id'],
                        'product_id' => $productTmp->id
                    ]);
                    if (isset($term['image'])) {
                        $path_picture_tmp = $term['image']->store('public');
                        $path_picture_tmp = str_replace('public', 'storage', $path_picture_tmp);
                        if ($path_picture_tmp) {
                            Picture::create([
                                'url' =>  $path_picture_tmp,
                                'product_id' =>  $productTmp->id,
                            ]);
                        }
                    }
                }
            }
            if ($request->hasFile('picture') && $path_picture) {
                Picture::create([
                    'url' =>  $path_picture,
                    'product_id' =>  $productTmp->id,
                ]);
            }

            return redirect()->route('admin.product.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->with('pictures')
            ->with('meta')->with('category:id,name')
            ->first();
        return Inertia::render("Dashboard/Product/Edit", [
            "Product" => $product,
            'Categories' => Category::select('id', 'name')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'productTypeCustomer' => 'required',
            'typeProduct' => 'required',
            'name' => 'required',
            'description' => 'required',
            'priceSimple' => 'required',
            'unitName' => 'required',
            'valueUnit' => 'required',
        ]);
        if ($request->hasFile('picture')) {
            $path_picture = $request->file('picture')->store('public');
            $path_picture = str_replace('public', 'storage', $path_picture);
        }
        if ($request->typeProduct == 'simple') {
            $productTmp = Product::where('id', $request->id)->update([
                'name' => $request->name,
                'category_id' => $request->category != null ? $request->category : 1,
                'product_type' => $request->typeProduct,
                'product_type_customer' => $request->productTypeCustomer,
            ]);
            Meta::where('product_id',  $request->id)->update([
                'origin' => $request->origin,
                'price' => $request->priceSimple,
                'stock' => 15,
                'unit' => $request->unitName,
                'value_unit' => $request->valueUnit,
                'few_units' => 5,
                'description' => $request->description,
            ]);
            if ($request->hasFile('picture') && $path_picture) {
                if (Picture::where('product_id', $request->id)->exists()) {
                    Picture::where('product_id', $request->id)->update([
                        'url' =>  $path_picture,
                    ]);
                } else {
                    Picture::create([
                        'url' =>  $path_picture,
                        'product_id' => $request->id
                    ]);
                }
            }

            return redirect()->route('admin.product.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return redirect()->back();
    }
}
