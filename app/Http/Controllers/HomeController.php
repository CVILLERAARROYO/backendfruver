<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = [
            [
                'image_url' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAACfCAMAAABX0UX9AAABOFBMVEX93ZnW2/gkPmDJ0fNDWmT/wFZBUFf////yV1X/tDb/5Jz/4pv/4JpDW2U5R00+V2MzUWEALVw8VV44VGLO1vkeO18tTmDxTFDR1/b/xFU+Vl8AMF3c4f8ALFwVN142UFgALFT215YAJlHs0JNNYmzyW1ffxo/zc2KfqsLJtom0ucILMFf0hGuzpoL/uDM1QUernX4AJlqDhHRobG6Gk6UAAD74yo/4sYL1k3LDyuSxutVUZWhQYXrxQ0yAgXNxd2/6xo1WXmlgcn9ygZHk5+2+rYRIVGWYj3phZmsuRWHGy9IxSGmPmarW2eLs7vCmrr1BVXMAF0picIhgcXu/xdM/U2ykrsj3qX72m3aNma+hi2LHoF/xul19eGQAFUqvkmC/mFjapEyPfFrdr2DmqETBlE3vrT9oamHdI0mOAAANoklEQVR4nO2dfVvTSBfGSxsIZqY0JU2B1r6mFEMBEco7Ut4rSIvvLu66K+4++v2/wTNnZpImaWhTcHXTzv2HinoJ/q5z5j5z5swQiQgJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCf1AYaQQIfyrv44wCivGauPo/Hy9aWL0q7+Y0EkxX+RqeV3X8/nkxSoWETiIcKSRy0ct6dkLU/nVX1KIhI0LDq/IftZzq4JfUGEjyqjli616Wae/FPwCC58zeuX6pdy+fsn4PTPF+hdIqJlliXspU+1Tfnr0V39dv1gYFODvlRi9/JUst+RTWW6X6cfZ5gjXL1iJVE3TNPrXcKhZY8HXktvP5Ktruf2Khd/F6GavUl2T0qqqpqWm0Qcges8Wu/Kp3HotX9d0bh7R5KiufjhykE5ITFq62ZtCKRe1l766fF22y7/8iGavYiY0IJfJUIDqeqkHQGxmebH8Bnyj9cYKvmi+MYr4cGQtDdQKmbfHbzPTEIDrQfBFi9fUeNv7nF9+fQRLP1SVIPQKld2xx6lU7EmchKC2dnccdfBFi/vvKMALfVTx4UgTQi9TuHn8eGwsFoulYseEX7p6Z/w58On5orzYkol9jCg+xQq9HQJvbGw2BqpIUqJxJwlc5dahR1+90uXrDVl+x/EdjBY+HnpSoU7hcXyp3QKxD6P7bysKC0neJXgv08Jloy0vMnzZ5ZEqXFC1AqE3zUMPRKMv9ZbgO/OiUDaXFrZgSVQajF+xDYXLVV2WT9jal+xGPrzCqElrvUJ9rCOG72Za0jw1HEbbM5OTM9sKLH5JlqsnbMsrnxZHb+lD1T2Vht7u4y58HwrexU/Zmif0Jiefl+CDcxZtxTqld5nnLau77WbYhPFqd+jZ3rEDi58j+lBkicIj4beJwTxY+EVrxRP5Mlpk9Gp3u82wyT/0bHwpUjunDSuWsPKRwyOiv4lWc9ZGwypaovmRaRhg5B96newlpZ9q7f+VzQWb3swh+02lafF784qXzBe9NnrDJGSs09DLeEOvg69uewdWfuvAW9i0MlpZfcbLF0YvezQi9OzQOxnzocfxPZkm3gGo8Oa8RW9mckvpIELGUY5bRlTP5kfloAMZbNWT/EKPKEX5QeEcB1Zo3g69pYirlMFK9eAil81mk9mj5chotFowWlZ7hJ7tHTHY9pYg+CzHnT/sii+MsGGemdUSndII2OgPs5DRUGl/4MMd8GzrhX0HeAfHN7Ptz4afjmCESlWzWsXKEBPECg+949k76Tm9Y5WwQOC6xDJ6Lm2KsfY+l8xmc9kXy2hY09gOvZse8JzecUBJLE3Of+wZVOTfTVomks/qy8NpItaq93anJz2Hd+yxEjnSO6CUsxovYdiPyWEsYXCpwbqiV73heb2jrxRrA6LvD28BrZhawtEV7Y8vVSHeEaAFgM4YPb2cly9f867p+5/wP/qJwqWDdIBVz7X4nWQkrX/3ExtZ3jo9hd7LNe9cDVXzAJmaFmTVc+G7Id7R47yIS3nB59R452+R8Rui83JcWgu46rmyl3hHYq9fEFmdq9q1LJ+04dCITVvpR8MSfvwEfDrAqufCFyPWq/XzDrTG2/Yt+XRDXtxfvOL2kRyKzilGbNXzbU31xgfe0eO0kknh1QqMa5TlxXKtZtV/q+HGhzFSIoa5Kmm+XdGesr2j+7jII8PqOpPkPZUXX9vTziEe1yDkUKRqNhuSqmqDhl4Hn89xkeNzsB+tpn00T423dVLkH4d18cOE3HJzXUurGp+YGjD0LHwxOC7yOzojYY1Kh7RDhavWvIEebbFTozL/+DyM+DA6axBwFrn7hN6Y67jI/a/DJaLNrd+W5mdmZuDEtxN90Wj5pdwm/Or58EYfOpNUi1xCU8E0pgcNPRtfKuPad2Alsrm1vUDAsRbW800nvlqtViTOS8KPpa/+IoT4mMtScvHvn37/g3xYmB2YnvO4yPYOdLgwaZFjHcCP5I9KFr7Fd1fP5MWNltzi+44QTrusg1Ek1PhnQm58jmiP4Nu9Nz7a8uPegQ+fT7o1s4Ud5+UkbUn0/dmZdunn2f85KQ2gl/78N4AbB819TkjTTwZPXqvlV+DHRURoyUtvHv4ErTJatUWZrHyXLeuOQrQWpFnzXxJaJamaqPzByDF8f2lS5uQe+LpHDVz4SBLPL7GmVImfkdfabM97FdbcLamE3p6T3vjc3+T3KvfGB95hjRrgrecc3MzC0sfDktVBVfgFGT1/3YYxZ1735UMXfE2SugkXvfHxPwi+wuD0/EYN0G/ENxa2Px5G3Nef0RHfadRek10H37LlzsK258CkYFH/ctMbn4sTfDv3xuf0DlImb0YU1HVxHJf4tjdaa1nTLsm1sKUurkLNMu4ReEfhx3jH3Z+5czWV08s1w0Yvgpc1KfF5zosPvKP+IO+Q+jecI42cdbEDlr18CI/a0Jomad7cHZ/7XZUyx/fGR4+LAszbKuZRkl/Lz+abpbCte0ToICGpv3vxjdN9x+D07FEDh3f0En0U4vzi/XrTDCM8iD6C70sXvvGHeQccFwXre+JwP0kCdYvWjW+uQbzj7nmWfvg6owZDLn/rGJ/7pEnT9/cOGDWohDSgBhI2VSnxvRvfA73D/3qM3+dHCgpx8tI9W/pR19oH3hEfnJ5z1CCAd2Cl2nxxfn5+1FgOp3VEEPxPb7vwjWs/wTsU8zyXh8JFz2ezB6EECJWLn3d8Tzyk5UdHDfpUwbi07iqbs2Ecc8arxHq/duP7Srwj6HiBU/aYWr9RA2zofNNWtjZtoetWce9YX+nC90V9SMvPOWrgP7pM6PGxtNft62f8kkII+RnEJLRu74CWX/z+Lb/OqAE63NoEa/UwtN8TumrJ7Xf7LABD+B4TJhuM9D/d/NIP9g42pjZPW6XbW5vIydB6T6jM3xM6YTCzobuZisgGQ/1fd/buPdQ76KgB4lcCZ2jTdGszwsPLek+oLsuXMpyUh7VZj8A7Pq14w48eF93cFx8dNQDv6NyO4QyX6CfF/KioeCm3N+TrK/k0rEdFbN+x8sjDjx0XgY/ODnbiG7Nbfmk4kVzwnrQtQHzZB5VsQK1ctp50Cd21fAzekXgEcvG7heMiVoYMgpA7b0pi3uFzzrsZ6RyTw0HlNcHXqf5Cl71Ypd7hAPgNfvkNvCPmVCoIQwufNaaGDumAhgPfIe4Maeiv6J38es2aUAvfkAb3jkcerUDLbzfm0Wy/OLRQ2zdTYV7w8OP2/CRnOK84J6xq/FmDE146h29EiHrH1258X8E7Ul5+fVJ51sL3wXlchBFlCBNWC+xSvv0gTu2CPcjEh3PDhw+8Q1K99B6t2N7hL1+Gs/Yfg3doLhuAvnKpRK9pdSasdF0vy4tkBeQzLuHDF8GaREsXD75baPndjc9vNXT8beoddxbB1nBu9OWb/aK8uNG2J6zCN5wL2Sulb738vsGowU4q1ZugIwxnnb/rGjXoksLf/tJlWrhsnFrzfbUwjobTxwvd/FZWVvbgVsdx/UOsP0LC0BuYrlEDr1DTvpggX7cXr+zp0jBeTMAmjBqofxFknNyj2y+fvsfpw4bThULl5GY3FYChC5/9qoHvZ+SLn75vvSfESr98+Ja+CB9SkzT10+0/377dfvm6l0675pyB4XH9SYBM7min53GR9ZxV7Q2dUHsX7veEGD8CMK2qaXqRjSkej3cYFgqZ45sPgRlOk83M3Z/RsAq94r58WePzaaF9T0g5UzvQbHKZiYmJjJOhlcpBlkMYdLn7EyLTeg+naN0HjObPf95/+AcLGWvpTr4CuSmup1PA0CHCEBylTxjSfn2PVFSWn/H423/F6YX6OjQylhsVsugBoSmvMjwg444wjPcKQ3De3hdTFTPvvoz/IhJiehG6t0IIGjDxLnwTcSk+8ZT8nHGuhjSVnwBD36Wvz9U2XDpI2lcBs9EQzqf5CO0lCCk/fJmnVOSXxGPsROep7AlDqJp7OQeTYjTf13LJZDK6fjYkb+HQPUjmqYfeFMQcwwcl4pnJrr91hyFlmNo5huALcCcfKSWDKDI0z+DQcd1O9k5wwdJnB59Er61Wz5qVtCsMWXEY261PwxPOAZ/UHLI3mHBFYtk74RTgmyL0pqA8ZJsxYFgyVxuOW5iZacKwUKAPiO+F7djix4heVchMeJQBppC6cfeaRuwGG2ZzzxGGVGojzFXIA0T7f5IX3wTDl4FTYW8jhYZhdfkArlFThgk1wFskQ6sEoPKKcMtMQeyp/vcH4C46hKGqqpWDsxF5l89PMO/cnb1sMwJZ2YMMCUNcCvG044+Qf/byNS29Nspogintk71025ZID8fe4F8VjEz6Zm+i0e/bEQmR7D3zy16y8O0NzebgX1XJL3ulYDethCLKuk/2SsGm5YXoZZmu7IVt7AgXw4PIUH2zt++0vBAVNP26sjce4GE+IRBcV+jKXmgWCHxBxO7od+OLC3yBhGnLXiTvPUWbfu7wA+cd0W/FObB8slfUfQMIQeUsuen1mhoQcolNXUnO1A1ydCbExR5Wkxyxl1gXK19wlSoJq8NMldBG9PDnfsJGxTl0pUmGoDeI4Js/O77veMgHeH6BlOqBCkonmlWx7g0ueM3ZXDaN0T47e4jwCHwjMCEhISEhISEhISEhISEhISEhISEhISEhISEhISEhoSHS/wE7XclnkNHDhwAAAABJRU5ErkJggg=='
            ],
            [
                'image_url' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAACfCAMAAABX0UX9AAABOFBMVEX93ZnW2/gkPmDJ0fNDWmT/wFZBUFf////yV1X/tDb/5Jz/4pv/4JpDW2U5R00+V2MzUWEALVw8VV44VGLO1vkeO18tTmDxTFDR1/b/xFU+Vl8AMF3c4f8ALFwVN142UFgALFT215YAJlHs0JNNYmzyW1ffxo/zc2KfqsLJtom0ucILMFf0hGuzpoL/uDM1QUernX4AJlqDhHRobG6Gk6UAAD74yo/4sYL1k3LDyuSxutVUZWhQYXrxQ0yAgXNxd2/6xo1WXmlgcn9ygZHk5+2+rYRIVGWYj3phZmsuRWHGy9IxSGmPmarW2eLs7vCmrr1BVXMAF0picIhgcXu/xdM/U2ykrsj3qX72m3aNma+hi2LHoF/xul19eGQAFUqvkmC/mFjapEyPfFrdr2DmqETBlE3vrT9oamHdI0mOAAANoklEQVR4nO2dfVvTSBfGSxsIZqY0JU2B1r6mFEMBEco7Ut4rSIvvLu66K+4++v2/wTNnZpImaWhTcHXTzv2HinoJ/q5z5j5z5swQiQgJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCf1AYaQQIfyrv44wCivGauPo/Hy9aWL0q7+Y0EkxX+RqeV3X8/nkxSoWETiIcKSRy0ct6dkLU/nVX1KIhI0LDq/IftZzq4JfUGEjyqjli616Wae/FPwCC58zeuX6pdy+fsn4PTPF+hdIqJlliXspU+1Tfnr0V39dv1gYFODvlRi9/JUst+RTWW6X6cfZ5gjXL1iJVE3TNPrXcKhZY8HXktvP5Ktruf2Khd/F6GavUl2T0qqqpqWm0Qcges8Wu/Kp3HotX9d0bh7R5KiufjhykE5ITFq62ZtCKRe1l766fF22y7/8iGavYiY0IJfJUIDqeqkHQGxmebH8Bnyj9cYKvmi+MYr4cGQtDdQKmbfHbzPTEIDrQfBFi9fUeNv7nF9+fQRLP1SVIPQKld2xx6lU7EmchKC2dnccdfBFi/vvKMALfVTx4UgTQi9TuHn8eGwsFoulYseEX7p6Z/w58On5orzYkol9jCg+xQq9HQJvbGw2BqpIUqJxJwlc5dahR1+90uXrDVl+x/EdjBY+HnpSoU7hcXyp3QKxD6P7bysKC0neJXgv08Jloy0vMnzZ5ZEqXFC1AqE3zUMPRKMv9ZbgO/OiUDaXFrZgSVQajF+xDYXLVV2WT9jal+xGPrzCqElrvUJ9rCOG72Za0jw1HEbbM5OTM9sKLH5JlqsnbMsrnxZHb+lD1T2Vht7u4y58HwrexU/Zmif0Jiefl+CDcxZtxTqld5nnLau77WbYhPFqd+jZ3rEDi58j+lBkicIj4beJwTxY+EVrxRP5Mlpk9Gp3u82wyT/0bHwpUjunDSuWsPKRwyOiv4lWc9ZGwypaovmRaRhg5B96newlpZ9q7f+VzQWb3swh+02lafF784qXzBe9NnrDJGSs09DLeEOvg69uewdWfuvAW9i0MlpZfcbLF0YvezQi9OzQOxnzocfxPZkm3gGo8Oa8RW9mckvpIELGUY5bRlTP5kfloAMZbNWT/EKPKEX5QeEcB1Zo3g69pYirlMFK9eAil81mk9mj5chotFowWlZ7hJ7tHTHY9pYg+CzHnT/sii+MsGGemdUSndII2OgPs5DRUGl/4MMd8GzrhX0HeAfHN7Ptz4afjmCESlWzWsXKEBPECg+949k76Tm9Y5WwQOC6xDJ6Lm2KsfY+l8xmc9kXy2hY09gOvZse8JzecUBJLE3Of+wZVOTfTVomks/qy8NpItaq93anJz2Hd+yxEjnSO6CUsxovYdiPyWEsYXCpwbqiV73heb2jrxRrA6LvD28BrZhawtEV7Y8vVSHeEaAFgM4YPb2cly9f867p+5/wP/qJwqWDdIBVz7X4nWQkrX/3ExtZ3jo9hd7LNe9cDVXzAJmaFmTVc+G7Id7R47yIS3nB59R452+R8Rui83JcWgu46rmyl3hHYq9fEFmdq9q1LJ+04dCITVvpR8MSfvwEfDrAqufCFyPWq/XzDrTG2/Yt+XRDXtxfvOL2kRyKzilGbNXzbU31xgfe0eO0kknh1QqMa5TlxXKtZtV/q+HGhzFSIoa5Kmm+XdGesr2j+7jII8PqOpPkPZUXX9vTziEe1yDkUKRqNhuSqmqDhl4Hn89xkeNzsB+tpn00T423dVLkH4d18cOE3HJzXUurGp+YGjD0LHwxOC7yOzojYY1Kh7RDhavWvIEebbFTozL/+DyM+DA6axBwFrn7hN6Y67jI/a/DJaLNrd+W5mdmZuDEtxN90Wj5pdwm/Or58EYfOpNUi1xCU8E0pgcNPRtfKuPad2Alsrm1vUDAsRbW800nvlqtViTOS8KPpa/+IoT4mMtScvHvn37/g3xYmB2YnvO4yPYOdLgwaZFjHcCP5I9KFr7Fd1fP5MWNltzi+44QTrusg1Ek1PhnQm58jmiP4Nu9Nz7a8uPegQ+fT7o1s4Ud5+UkbUn0/dmZdunn2f85KQ2gl/78N4AbB819TkjTTwZPXqvlV+DHRURoyUtvHv4ErTJatUWZrHyXLeuOQrQWpFnzXxJaJamaqPzByDF8f2lS5uQe+LpHDVz4SBLPL7GmVImfkdfabM97FdbcLamE3p6T3vjc3+T3KvfGB95hjRrgrecc3MzC0sfDktVBVfgFGT1/3YYxZ1735UMXfE2SugkXvfHxPwi+wuD0/EYN0G/ENxa2Px5G3Nef0RHfadRek10H37LlzsK258CkYFH/ctMbn4sTfDv3xuf0DlImb0YU1HVxHJf4tjdaa1nTLsm1sKUurkLNMu4ReEfhx3jH3Z+5czWV08s1w0Yvgpc1KfF5zosPvKP+IO+Q+jecI42cdbEDlr18CI/a0Jomad7cHZ/7XZUyx/fGR4+LAszbKuZRkl/Lz+abpbCte0ToICGpv3vxjdN9x+D07FEDh3f0En0U4vzi/XrTDCM8iD6C70sXvvGHeQccFwXre+JwP0kCdYvWjW+uQbzj7nmWfvg6owZDLn/rGJ/7pEnT9/cOGDWohDSgBhI2VSnxvRvfA73D/3qM3+dHCgpx8tI9W/pR19oH3hEfnJ5z1CCAd2Cl2nxxfn5+1FgOp3VEEPxPb7vwjWs/wTsU8zyXh8JFz2ezB6EECJWLn3d8Tzyk5UdHDfpUwbi07iqbs2Ecc8arxHq/duP7Srwj6HiBU/aYWr9RA2zofNNWtjZtoetWce9YX+nC90V9SMvPOWrgP7pM6PGxtNft62f8kkII+RnEJLRu74CWX/z+Lb/OqAE63NoEa/UwtN8TumrJ7Xf7LABD+B4TJhuM9D/d/NIP9g42pjZPW6XbW5vIydB6T6jM3xM6YTCzobuZisgGQ/1fd/buPdQ76KgB4lcCZ2jTdGszwsPLek+oLsuXMpyUh7VZj8A7Pq14w48eF93cFx8dNQDv6NyO4QyX6CfF/KioeCm3N+TrK/k0rEdFbN+x8sjDjx0XgY/ODnbiG7Nbfmk4kVzwnrQtQHzZB5VsQK1ctp50Cd21fAzekXgEcvG7heMiVoYMgpA7b0pi3uFzzrsZ6RyTw0HlNcHXqf5Cl71Ypd7hAPgNfvkNvCPmVCoIQwufNaaGDumAhgPfIe4Maeiv6J38es2aUAvfkAb3jkcerUDLbzfm0Wy/OLRQ2zdTYV7w8OP2/CRnOK84J6xq/FmDE146h29EiHrH1258X8E7Ul5+fVJ51sL3wXlchBFlCBNWC+xSvv0gTu2CPcjEh3PDhw+8Q1K99B6t2N7hL1+Gs/Yfg3doLhuAvnKpRK9pdSasdF0vy4tkBeQzLuHDF8GaREsXD75baPndjc9vNXT8beoddxbB1nBu9OWb/aK8uNG2J6zCN5wL2Sulb738vsGowU4q1ZugIwxnnb/rGjXoksLf/tJlWrhsnFrzfbUwjobTxwvd/FZWVvbgVsdx/UOsP0LC0BuYrlEDr1DTvpggX7cXr+zp0jBeTMAmjBqofxFknNyj2y+fvsfpw4bThULl5GY3FYChC5/9qoHvZ+SLn75vvSfESr98+Ja+CB9SkzT10+0/377dfvm6l0675pyB4XH9SYBM7min53GR9ZxV7Q2dUHsX7veEGD8CMK2qaXqRjSkej3cYFgqZ45sPgRlOk83M3Z/RsAq94r58WePzaaF9T0g5UzvQbHKZiYmJjJOhlcpBlkMYdLn7EyLTeg+naN0HjObPf95/+AcLGWvpTr4CuSmup1PA0CHCEBylTxjSfn2PVFSWn/H423/F6YX6OjQylhsVsugBoSmvMjwg444wjPcKQ3De3hdTFTPvvoz/IhJiehG6t0IIGjDxLnwTcSk+8ZT8nHGuhjSVnwBD36Wvz9U2XDpI2lcBs9EQzqf5CO0lCCk/fJmnVOSXxGPsROep7AlDqJp7OQeTYjTf13LJZDK6fjYkb+HQPUjmqYfeFMQcwwcl4pnJrr91hyFlmNo5huALcCcfKSWDKDI0z+DQcd1O9k5wwdJnB59Er61Wz5qVtCsMWXEY261PwxPOAZ/UHLI3mHBFYtk74RTgmyL0pqA8ZJsxYFgyVxuOW5iZacKwUKAPiO+F7djix4heVchMeJQBppC6cfeaRuwGG2ZzzxGGVGojzFXIA0T7f5IX3wTDl4FTYW8jhYZhdfkArlFThgk1wFskQ6sEoPKKcMtMQeyp/vcH4C46hKGqqpWDsxF5l89PMO/cnb1sMwJZ2YMMCUNcCvG044+Qf/byNS29Nspogintk71025ZID8fe4F8VjEz6Zm+i0e/bEQmR7D3zy16y8O0NzebgX1XJL3ulYDethCLKuk/2SsGm5YXoZZmu7IVt7AgXw4PIUH2zt++0vBAVNP26sjce4GE+IRBcV+jKXmgWCHxBxO7od+OLC3yBhGnLXiTvPUWbfu7wA+cd0W/FObB8slfUfQMIQeUsuen1mhoQcolNXUnO1A1ydCbExR5Wkxyxl1gXK19wlSoJq8NMldBG9PDnfsJGxTl0pUmGoDeI4Js/O77veMgHeH6BlOqBCkonmlWx7g0ueM3ZXDaN0T47e4jwCHwjMCEhISEhISEhISEhISEhISEhISEhISEhISEhISEhoSHS/wE7XclnkNHDhwAAAABJRU5ErkJggg=='
            ],
            [
                'image_url' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAACfCAMAAABX0UX9AAABOFBMVEX93ZnW2/gkPmDJ0fNDWmT/wFZBUFf////yV1X/tDb/5Jz/4pv/4JpDW2U5R00+V2MzUWEALVw8VV44VGLO1vkeO18tTmDxTFDR1/b/xFU+Vl8AMF3c4f8ALFwVN142UFgALFT215YAJlHs0JNNYmzyW1ffxo/zc2KfqsLJtom0ucILMFf0hGuzpoL/uDM1QUernX4AJlqDhHRobG6Gk6UAAD74yo/4sYL1k3LDyuSxutVUZWhQYXrxQ0yAgXNxd2/6xo1WXmlgcn9ygZHk5+2+rYRIVGWYj3phZmsuRWHGy9IxSGmPmarW2eLs7vCmrr1BVXMAF0picIhgcXu/xdM/U2ykrsj3qX72m3aNma+hi2LHoF/xul19eGQAFUqvkmC/mFjapEyPfFrdr2DmqETBlE3vrT9oamHdI0mOAAANoklEQVR4nO2dfVvTSBfGSxsIZqY0JU2B1r6mFEMBEco7Ut4rSIvvLu66K+4++v2/wTNnZpImaWhTcHXTzv2HinoJ/q5z5j5z5swQiQgJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCf1AYaQQIfyrv44wCivGauPo/Hy9aWL0q7+Y0EkxX+RqeV3X8/nkxSoWETiIcKSRy0ct6dkLU/nVX1KIhI0LDq/IftZzq4JfUGEjyqjli616Wae/FPwCC58zeuX6pdy+fsn4PTPF+hdIqJlliXspU+1Tfnr0V39dv1gYFODvlRi9/JUst+RTWW6X6cfZ5gjXL1iJVE3TNPrXcKhZY8HXktvP5Ktruf2Khd/F6GavUl2T0qqqpqWm0Qcges8Wu/Kp3HotX9d0bh7R5KiufjhykE5ITFq62ZtCKRe1l766fF22y7/8iGavYiY0IJfJUIDqeqkHQGxmebH8Bnyj9cYKvmi+MYr4cGQtDdQKmbfHbzPTEIDrQfBFi9fUeNv7nF9+fQRLP1SVIPQKld2xx6lU7EmchKC2dnccdfBFi/vvKMALfVTx4UgTQi9TuHn8eGwsFoulYseEX7p6Z/w58On5orzYkol9jCg+xQq9HQJvbGw2BqpIUqJxJwlc5dahR1+90uXrDVl+x/EdjBY+HnpSoU7hcXyp3QKxD6P7bysKC0neJXgv08Jloy0vMnzZ5ZEqXFC1AqE3zUMPRKMv9ZbgO/OiUDaXFrZgSVQajF+xDYXLVV2WT9jal+xGPrzCqElrvUJ9rCOG72Za0jw1HEbbM5OTM9sKLH5JlqsnbMsrnxZHb+lD1T2Vht7u4y58HwrexU/Zmif0Jiefl+CDcxZtxTqld5nnLau77WbYhPFqd+jZ3rEDi58j+lBkicIj4beJwTxY+EVrxRP5Mlpk9Gp3u82wyT/0bHwpUjunDSuWsPKRwyOiv4lWc9ZGwypaovmRaRhg5B96newlpZ9q7f+VzQWb3swh+02lafF784qXzBe9NnrDJGSs09DLeEOvg69uewdWfuvAW9i0MlpZfcbLF0YvezQi9OzQOxnzocfxPZkm3gGo8Oa8RW9mckvpIELGUY5bRlTP5kfloAMZbNWT/EKPKEX5QeEcB1Zo3g69pYirlMFK9eAil81mk9mj5chotFowWlZ7hJ7tHTHY9pYg+CzHnT/sii+MsGGemdUSndII2OgPs5DRUGl/4MMd8GzrhX0HeAfHN7Ptz4afjmCESlWzWsXKEBPECg+949k76Tm9Y5WwQOC6xDJ6Lm2KsfY+l8xmc9kXy2hY09gOvZse8JzecUBJLE3Of+wZVOTfTVomks/qy8NpItaq93anJz2Hd+yxEjnSO6CUsxovYdiPyWEsYXCpwbqiV73heb2jrxRrA6LvD28BrZhawtEV7Y8vVSHeEaAFgM4YPb2cly9f867p+5/wP/qJwqWDdIBVz7X4nWQkrX/3ExtZ3jo9hd7LNe9cDVXzAJmaFmTVc+G7Id7R47yIS3nB59R452+R8Rui83JcWgu46rmyl3hHYq9fEFmdq9q1LJ+04dCITVvpR8MSfvwEfDrAqufCFyPWq/XzDrTG2/Yt+XRDXtxfvOL2kRyKzilGbNXzbU31xgfe0eO0kknh1QqMa5TlxXKtZtV/q+HGhzFSIoa5Kmm+XdGesr2j+7jII8PqOpPkPZUXX9vTziEe1yDkUKRqNhuSqmqDhl4Hn89xkeNzsB+tpn00T423dVLkH4d18cOE3HJzXUurGp+YGjD0LHwxOC7yOzojYY1Kh7RDhavWvIEebbFTozL/+DyM+DA6axBwFrn7hN6Y67jI/a/DJaLNrd+W5mdmZuDEtxN90Wj5pdwm/Or58EYfOpNUi1xCU8E0pgcNPRtfKuPad2Alsrm1vUDAsRbW800nvlqtViTOS8KPpa/+IoT4mMtScvHvn37/g3xYmB2YnvO4yPYOdLgwaZFjHcCP5I9KFr7Fd1fP5MWNltzi+44QTrusg1Ek1PhnQm58jmiP4Nu9Nz7a8uPegQ+fT7o1s4Ud5+UkbUn0/dmZdunn2f85KQ2gl/78N4AbB819TkjTTwZPXqvlV+DHRURoyUtvHv4ErTJatUWZrHyXLeuOQrQWpFnzXxJaJamaqPzByDF8f2lS5uQe+LpHDVz4SBLPL7GmVImfkdfabM97FdbcLamE3p6T3vjc3+T3KvfGB95hjRrgrecc3MzC0sfDktVBVfgFGT1/3YYxZ1735UMXfE2SugkXvfHxPwi+wuD0/EYN0G/ENxa2Px5G3Nef0RHfadRek10H37LlzsK258CkYFH/ctMbn4sTfDv3xuf0DlImb0YU1HVxHJf4tjdaa1nTLsm1sKUurkLNMu4ReEfhx3jH3Z+5czWV08s1w0Yvgpc1KfF5zosPvKP+IO+Q+jecI42cdbEDlr18CI/a0Jomad7cHZ/7XZUyx/fGR4+LAszbKuZRkl/Lz+abpbCte0ToICGpv3vxjdN9x+D07FEDh3f0En0U4vzi/XrTDCM8iD6C70sXvvGHeQccFwXre+JwP0kCdYvWjW+uQbzj7nmWfvg6owZDLn/rGJ/7pEnT9/cOGDWohDSgBhI2VSnxvRvfA73D/3qM3+dHCgpx8tI9W/pR19oH3hEfnJ5z1CCAd2Cl2nxxfn5+1FgOp3VEEPxPb7vwjWs/wTsU8zyXh8JFz2ezB6EECJWLn3d8Tzyk5UdHDfpUwbi07iqbs2Ecc8arxHq/duP7Srwj6HiBU/aYWr9RA2zofNNWtjZtoetWce9YX+nC90V9SMvPOWrgP7pM6PGxtNft62f8kkII+RnEJLRu74CWX/z+Lb/OqAE63NoEa/UwtN8TumrJ7Xf7LABD+B4TJhuM9D/d/NIP9g42pjZPW6XbW5vIydB6T6jM3xM6YTCzobuZisgGQ/1fd/buPdQ76KgB4lcCZ2jTdGszwsPLek+oLsuXMpyUh7VZj8A7Pq14w48eF93cFx8dNQDv6NyO4QyX6CfF/KioeCm3N+TrK/k0rEdFbN+x8sjDjx0XgY/ODnbiG7Nbfmk4kVzwnrQtQHzZB5VsQK1ctp50Cd21fAzekXgEcvG7heMiVoYMgpA7b0pi3uFzzrsZ6RyTw0HlNcHXqf5Cl71Ypd7hAPgNfvkNvCPmVCoIQwufNaaGDumAhgPfIe4Maeiv6J38es2aUAvfkAb3jkcerUDLbzfm0Wy/OLRQ2zdTYV7w8OP2/CRnOK84J6xq/FmDE146h29EiHrH1258X8E7Ul5+fVJ51sL3wXlchBFlCBNWC+xSvv0gTu2CPcjEh3PDhw+8Q1K99B6t2N7hL1+Gs/Yfg3doLhuAvnKpRK9pdSasdF0vy4tkBeQzLuHDF8GaREsXD75baPndjc9vNXT8beoddxbB1nBu9OWb/aK8uNG2J6zCN5wL2Sulb738vsGowU4q1ZugIwxnnb/rGjXoksLf/tJlWrhsnFrzfbUwjobTxwvd/FZWVvbgVsdx/UOsP0LC0BuYrlEDr1DTvpggX7cXr+zp0jBeTMAmjBqofxFknNyj2y+fvsfpw4bThULl5GY3FYChC5/9qoHvZ+SLn75vvSfESr98+Ja+CB9SkzT10+0/377dfvm6l0675pyB4XH9SYBM7min53GR9ZxV7Q2dUHsX7veEGD8CMK2qaXqRjSkej3cYFgqZ45sPgRlOk83M3Z/RsAq94r58WePzaaF9T0g5UzvQbHKZiYmJjJOhlcpBlkMYdLn7EyLTeg+naN0HjObPf95/+AcLGWvpTr4CuSmup1PA0CHCEBylTxjSfn2PVFSWn/H423/F6YX6OjQylhsVsugBoSmvMjwg444wjPcKQ3De3hdTFTPvvoz/IhJiehG6t0IIGjDxLnwTcSk+8ZT8nHGuhjSVnwBD36Wvz9U2XDpI2lcBs9EQzqf5CO0lCCk/fJmnVOSXxGPsROep7AlDqJp7OQeTYjTf13LJZDK6fjYkb+HQPUjmqYfeFMQcwwcl4pnJrr91hyFlmNo5huALcCcfKSWDKDI0z+DQcd1O9k5wwdJnB59Er61Wz5qVtCsMWXEY261PwxPOAZ/UHLI3mHBFYtk74RTgmyL0pqA8ZJsxYFgyVxuOW5iZacKwUKAPiO+F7djix4heVchMeJQBppC6cfeaRuwGG2ZzzxGGVGojzFXIA0T7f5IX3wTDl4FTYW8jhYZhdfkArlFThgk1wFskQ6sEoPKKcMtMQeyp/vcH4C46hKGqqpWDsxF5l89PMO/cnb1sMwJZ2YMMCUNcCvG044+Qf/byNS29Nspogintk71025ZID8fe4F8VjEz6Zm+i0e/bEQmR7D3zy16y8O0NzebgX1XJL3ulYDethCLKuk/2SsGm5YXoZZmu7IVt7AgXw4PIUH2zt++0vBAVNP26sjce4GE+IRBcV+jKXmgWCHxBxO7od+OLC3yBhGnLXiTvPUWbfu7wA+cd0W/FObB8slfUfQMIQeUsuen1mhoQcolNXUnO1A1ydCbExR5Wkxyxl1gXK19wlSoJq8NMldBG9PDnfsJGxTl0pUmGoDeI4Js/O77veMgHeH6BlOqBCkonmlWx7g0ueM3ZXDaN0T47e4jwCHwjMCEhISEhISEhISEhISEhISEhISEhISEhISEhISEhoSHS/wE7XclnkNHDhwAAAABJRU5ErkJggg=='
            ]
        ];
        $categories = Category::with('products.value.term:id,name')
            ->with('products.meta')
            ->with('products.pictures')
            ->inRandomOrder()
            ->take(3)
            ->where('id', '!=', 1)
            ->get()
            ->map(function ($query) {
                $query->setRelation('products', $query->products->take(12));
                return $query;
            });
        $products = Product::with('values.term')
            ->with('pictures')
            ->with('meta')
            ->take(12)
            ->orderBy('id', 'DESC')
            ->get();

        return Inertia::render('Home/Index', [
            'Offers' => $offers,
            'Products' =>  $products,
            'Categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
