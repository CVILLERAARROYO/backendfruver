<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth()->user()) {
            $user = Auth()->user();
            return Inertia::render('Orders/Index', [
                'Orders' => $user->orders
            ]);
        } else {
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            if ($request->shipping['typeShipping'] == 'normal') {
                $order = $user->orders()->create([
                    'order_number' => rand(5, 15),
                    'sub_total' => $request->subTotal,
                    'total'  => $request->total,
                    'shippingBasic' => $request->shippingBasic,
                    'shippingExpress' => $request->shipping['express'] ? $request->shippingExpress : 0,
                    'observation'  => $request->observation,

                ]);
                $billing = $order->billing()->create([
                    'name' => $request->billing['name'],
                    'last_name' => $request->billing['last_name'],
                    'phone' => $request->billing['phone'],
                    'email' => $request->billing['email'],
                    'document' => $request->billing['document'],
                    'user_id' => Auth()->user()->id
                ]);
                $order->update([
                    'billing_id' => $billing->id
                ]);

                $shipping = $order->shipping()->create([
                    'express'  => $request->shipping['express'],
                    'country' => $request->shipping['country'],
                    'department' => $request->shipping['department'],
                    'address' => $request->shipping['address'],
                    'reference' => $request->shipping['reference'],
                    'user_id' => Auth()->user()->id
                ]);
                $order->update([
                    'shipping_id' => $shipping->id
                ]);
                foreach ($request->items as $value) {
                    $order->detail()->create([
                        'quantity' => $value['quantity'],
                        'product_id' => $value['id'],
                        'total' => $value['price'] * $value['quantity']
                    ]);
                }
            } elseif ($request->shipping['typeShipping'] == 'store') {
                $order = $user->orders()->create([
                    'order_number' => rand(5, 15),
                    'sub_total' => $request->subTotal,
                    'total'  => $request->total,
                ]);
                $billing = $order->billing()->create([
                    'name' => $request->billing['name'],
                    'last_name' => $request->billing['last_name'],
                    'phone' => $request->billing['phone'],
                    'email' => $request->billing['email'],
                    'document' => $request->billing['document'],
                    'user_id' => Auth()->user()->id
                ]);
                $order->update([
                    'billing_id' => $billing->id
                ]);


                foreach ($request->items as $value) {
                    $order->detail()->create([
                        'quantity' => $value['quantity'],
                        'product_id' => $value['id'],
                        'total' => $value['price'] * $value['quantity']
                    ]);
                }
            }
        } else {

            if ($request->shipping['typeShipping'] == 'normal') {
                $order = Order::create([
                    'order_number' => rand(5, 15),
                    'sub_total' => $request->subTotal,
                    'total'  => $request->total,
                    'shippingBasic' => $request->shippingBasic,
                    'shippingExpress' => $request->shippingExpress,
                ]);
                $billing = $order->billing()->create([
                    'name' => $request->billing['name'],
                    'last_name' => $request->billing['last_name'],
                    'phone' => $request->billing['phone'],
                    'email' => $request->billing['email'],
                    'document' => $request->billing['document'],
                ]);
                $order->update([
                    'billing_id' => $billing->id
                ]);

                // $shipping = $order->shipping()->create([
                //     'express'  => $request->shipping['express'],
                //     'country' => $request->shipping['country'],
                //     'department' => $request->shipping['department'],
                //     'address' => $request->shipping['address'],
                //     'reference' => $request->shipping['reference'],
                // ]);

                // $order->update([
                //     'shipping_id' => $shipping->id
                // ]);
                foreach ($request->items as $value) {
                    $order->detail()->create([
                        'quantity' => $value['quantity'],
                        'product_id' => $value['id'],
                        'total' => $value['price'] * $value['quantity']
                    ]);
                }
            }
        }

        \Cart::clear();
        return redirect('pedidos/' . $order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $order = Order::where('id', $id)
            ->with('detail')
            ->with('billing')
            ->with('shipping')
            ->first();

        $productIds = [];

        foreach ($order->detail as $value) {
            array_push($productIds, $value->product_id);
        }

        $products = Product::with('values.term')
            ->with('pictures')
            ->with('meta')
            ->whereIn('id', $productIds)
            ->get();
        return Inertia::render('Orders/Show', [
            'Order' => $order,
            'Products' => $products,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
