<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        // $products = [
        //     [
        //         'name' => 'Mango',
        //         'price' => '4000',
        //         'unit' => 'Kg',
        //         'image_url' => 'https://exoticfruitbox.com/wp-content/uploads/2015/10/mango.jpg'
        //     ],
        //     [
        //         'name' => 'Mango',
        //         'price' => '4000',
        //         'unit' => 'Kg',
        //         'image_url' => 'https://exoticfruitbox.com/wp-content/uploads/2015/10/mango.jpg'
        //     ],
        //     [
        //         'name' => 'Mango',
        //         'price' => '4000',
        //         'unit' => 'Kg',
        //         'image_url' => 'https://exoticfruitbox.com/wp-content/uploads/2015/10/mango.jpg'
        //     ]

        // ];
        // $producAAAt = [

        //     'id' => '1',
        //     'name' => 'Mango',
        //     'sku' => '123531',
        //     'price' => '50000',
        //     'units' => [
        //         [
        //             'name' => 'Libra',
        //             'value' => '1000'
        //         ],
        //         [
        //             'name' => 'Kilogramo',
        //             'value' => '1800'
        //         ]
        //     ],
        //     'stock' => '23',
        //     'description' => ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Lauur necessitatibus sapiente, deserunt corrupti expedita quae minima nemo. Quis porro aperiam non repellat at.',
        //     'image_url' => 'https://exoticfruitbox.com/wp-content/uploads/2015/10/mango.jpg',
        //     'category' => [
        //         'id' => '2',
        //         'name' => 'Frutas'
        //     ],
        //     'origin' => 'Santa Elena'

        // ];
        // if (Auth::check()) {
        //     $product = Product::with('values.term')
        //     ->where('id',$id)
        //     ->where('product_type_customer', 'mayorista')
        //     ->with('category')
        //     ->first();
        // }else{
        //     $product = Product::with('values.term')
        //     ->where('id',$id)
        //     ->where('product_type_customer', 'minorista')
        //     ->with('category')
        //     ->first();
        // }
        $product = Product::with('values.term')
        ->with('pictures')
        ->with('meta')
        ->with('category')
        ->where('slug',$slug)
        ->firstOrFail();
        $products = Product::with('values.term')
        ->with('pictures')
        ->with('meta')
        ->take(6)
        ->orderBy('id', 'DESC')
        ->get();
        return Inertia::render('Product/Index', [
            'Product' => $product,
            'Products' => $products

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
