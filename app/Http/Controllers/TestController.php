<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class TestController extends Controller
{
    public function __construct()
    {
        // $this->middleware(['role_or_permission:supersAdmin|dashboard']);
    }
    public function index()
    {
        return Inertia::render('Dashboard');
    }
}
