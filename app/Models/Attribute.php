<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;
    protected $hidden = ['pivot'];
    protected $fillable = [
        'name',
        'description',
        'created_by_id',
    ];
    /**
     * Products data that belong to the Attribute
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    /**
     * Terms that belong to the attribute
     */
    public function terms()
    {
        return $this->hasMany(Term::class);
    }
}
