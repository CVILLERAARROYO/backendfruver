<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'last_name',
        'phone',
        'email',
        'document',
        'user_id',
        'order_id'
    ];
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
