<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Sluggable;
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'slug',
        'color',
        'description',
        'created_by_id',
        'url_icon',
        'url_banner',
        'url_banner_desktop',
        'parent_id',
    ];
    /**
     * Default attribute in Model.
     */
    protected static function boot()
    {
        parent::boot();
        if (auth()->check()) {
            self::creating(function ($model) {
                $model->created_by_id = Auth::id();
            });
        }
    }
    /**
     * Get the category image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
