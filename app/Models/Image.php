<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Image extends Model
{
    use HasFactory;
    protected $hidden = [
        'id',
        'imageable_type',
        'imageable_id',
        'created_by_id',
        'updated_at',
        'created_at',
        'deleted_at',
    ];
    protected $fillable = [
        'url',
        'imageable_type',
        'imageable_id'
    ];
    /**
     * Default attribute in Model.
     */
    protected static function boot()
    {
        parent::boot();
        if (auth()->check()) {
            self::creating(function ($model) {
                $model->created_by_id = Auth::id();
            });
        }
    }
    public function imageable()
    {
        return $this->morphTo();
    }
}
