<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    use HasFactory;

    /**
     * Set model table.
     *
     * @var string[]
     */
    protected $table = "product_metas";
    public $timestamps = false;

    protected $hidden = [
        'id',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'origin',
        'price',
        'stock',
        'few_units',
        'description',
        'product_id',
        'unit',
        'value_unit',

    ];
    /**
     * Product to which the meta belongs
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
