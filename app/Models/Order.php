<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_number',
        'sub_total',
        'total',
        'shippingBasic',
        'shippingExpress',
        'billing_id',
        'shipping_id',
        'user_id',
        'detail_id',
        'created_at',
        'observation'
    ];
   
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function detail()
    {
        return $this->hasMany(Detail::class);
    }
    public function billing()
    {
        return $this->hasOne(Billing::class);
    }
    public function shipping()
    {
        return $this->hasOne(Shipping::class);
    }

}
