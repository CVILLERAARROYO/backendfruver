<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    use HasFactory;

    protected $hidden = [
        'id',
        'updated_at',
        'created_at',
        'product_id',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'url',
        'product_id',
    ];
    /**
     * Product to which the picture belongs
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
