<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Cviebrock\EloquentSluggable\Sluggable;



class Product extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;
    use SoftDeletes;
    use Sluggable;
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $hidden = ['pivot'];
    protected $fillable = [
        'name',
        'sku',
        'slug',
        'price',
        'product_type',
        'product_status',
        'product_type_customer',
        'category_id',
    ];
    protected static function boot()
    {
        parent::boot();
        if (auth()->check()) {
            self::creating(function ($model) {
                $model->created_by_id = Auth::id();
            });
        }
    }
    /**
     * Category to which the product belongs
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * Meta data that belong to the product
     */
    public function meta()
    {
        return $this->hasOne(Meta::class);
    }
    /**
     * Picture data that belong to the product
     */
    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }
    /**
     * Attributes data that belong to the product
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    public function values()
    {
        return $this->hasMany(Value::class);

    }
    public function value()
    {
        return $this->hasOne(Value::class);

    }
    public function terms()
    {
        return $this->belongsToMany(Term::class);
    }
}
