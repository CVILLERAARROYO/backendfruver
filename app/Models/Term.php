<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Value;

class Term extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'description',
        'attribute_id'
    ];
    /**
     * Attribute to which the term belongs
     */
    public function Attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
     /**
     * Values data that belong to the term
     */
    public function values()
    {
        return $this->hasMany(Value::class);
    }
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
