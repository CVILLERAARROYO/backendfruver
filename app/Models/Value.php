<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    use HasFactory;
      /**
     * Set model table.
     *
     * @var string[]
     */
    protected $table = "term_values";

    protected $hidden = [
        'id',
        'updated_at',
        'created_at',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'price',
        'sku',
        'product_status',
        'stock',
        'few_units',
        'term_id',
        'product_id',
    ];
     /**
     * Term to which the value belongs
     */
    public function term()
    {
        return $this->belongsTo(Term::class);
    }



}
