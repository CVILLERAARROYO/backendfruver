<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google' => [
        'client_id' => '712738934020-rfgdhv1q4mmdp750v5ar3f2vovmnrer6.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'http://disfrutasmifinca.com/auth/google/callback',
    ],
    'facebook' => [
        'client_id' => '2934604516810029',
        'client_secret' => '6f4f9c46ab7e0506203d3738aaef3b56',
        'redirect' => 'https://disfrutasmifinca.com/auth/facebook/callback',
    ],

];
