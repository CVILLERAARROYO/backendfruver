<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term_values', function (Blueprint $table) {
            $table->id();
            $table->string('price');
            $table->string('sku');
            $table->string('product_status');
            $table->string('origin')->nullable();
            $table->integer('stock')->nullable();
            $table->integer('few_units')->nullable();
            $table->foreignId('term_id')
                ->nullable()
                ->constrained('terms')
                ->onDelete('cascade');
            $table->foreignId('product_id')
                ->nullable()
                ->constrained('products')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('term_values');
    }
}
