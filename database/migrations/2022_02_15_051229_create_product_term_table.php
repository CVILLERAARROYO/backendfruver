<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_term', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')
            ->nullable()
            ->constrained('products')
            ->onDelete('cascade');
            $table->foreignId('term_id')
            ->nullable()
            ->constrained('terms')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_term');
    }
}
