<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_number');
            $table->string('sub_total');
            $table->string('total');
            $table->foreignId('billing_id')
            ->nullable()
            ->constrained('billings')
            ->onDelete('set null');
            $table->foreignId('shipping_id')
            ->nullable()
            ->constrained('shippings')
            ->onDelete('set null');
            $table->foreignId('user_id')
            ->nullable()
            ->constrained('users')
            ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
