<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Carlos',
            'last_name' => 'Villera',
            'email' => 'carlosmva2009@hotmail.com',
            'password' => Hash::make('Arroyocarlos_18'),
        ])->assignRole('superAdmin');
        Category::create([
            'name' => 'Sin categoria',
            'color' => "#79B942",
        ]);
    }
}
