<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SocialiteController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])
    ->name('home');
Route::get('/dashboard', [App\Http\Controllers\TestController::class, 'index'])
    ->name('dashboard')
    ->middleware(['auth:sanctum']);

// Route::get('producto/{slug}', [ProductController::class, 'show'])
// ->name('product.show');
Route::resource('producto', ProductController::class)->names([
    'show' => 'product.show'
]);
// Categories
Route::resource('categoria', CategoryController::class)->names([
    'index' => 'category.index',
    'create' => 'category.create',
    'store' => 'category.store'
]);
// Cart
Route::resource('carrito', CartController::class)->names([
    'index' => 'cart.index',
    'store' => 'cart.store',
    'destroy' => 'cart.destroy'
]);
Route::post('carrito/update', [CartController::class, 'update'])
    ->name('cart.update');

// Checkout
Route::resource('finalizar-compra', CheckoutController::class)->names([
    'index' => 'checkout.index',
]);
// Address
Route::resource('direcciones', AddressController::class)->names([
    'index' => 'address.index',
    'destroy' => 'address.destroy',
    'store' => 'address.store',
]);
Route::post('direcciones/update', [AddressController::class, 'update'])->name('address.update');
// Orders
Route::resource('pedidos', OrderController::class)->names([
    'index' => 'order.index',
    'show' => 'order.show',
    'destroy' => 'order.destroy',
    'store' => 'order.store',
]);
// search
Route::post('search', [SearchController::class, 'index'])->name('search.index');
//dashboard
Route::get('/dashboard-admin', [App\Http\Controllers\Dashboard\DashboardController::class, 'index'])
    ->name('admin.index')
    ->middleware(['auth:sanctum']);
//dashboard profile
Route::get('/dashboard-usuarios', [App\Http\Controllers\Dashboard\ProfileController::class, 'index'])
    ->name('admin.profile.index')
    ->middleware(['auth:sanctum']);

Route::get('/dashboard-usuarios-crear', [App\Http\Controllers\Dashboard\ProfileController::class, 'create'])
    ->name('admin.profile.create')
    ->middleware(['auth:sanctum']);

Route::post('/dashboard-usuarios-store', [App\Http\Controllers\Dashboard\ProfileController::class, 'store'])
    ->name('admin.profile.store')
    ->middleware(['auth:sanctum']);
//dashboard role
Route::get('/dashboard-roles', [App\Http\Controllers\Dashboard\RoleController::class, 'index'])
    ->name('admin.role.index')
    ->middleware(['auth:sanctum']);

//dashboard product terms
Route::get('/dashboard-productos/terminos', [App\Http\Controllers\Dashboard\TermController::class, 'index'])
    ->name('admin.product.terms.index')
    ->middleware(['auth:sanctum']);
Route::post('/dashboard-productos-terminos-guardar', [App\Http\Controllers\Dashboard\TermController::class, 'store'])
    ->name('admin.product.terms.store')
    ->middleware(['auth:sanctum']);
Route::delete('/dashboard-productos-terminos-delete/{product}', [App\Http\Controllers\Dashboard\TermController::class, 'destroy'])
    ->name('admin.terms.destroy')
    ->middleware(['auth:sanctum']);
//dashboard product attributes
Route::get('/dashboard-productos/atributos', [App\Http\Controllers\Dashboard\AttributeController::class, 'index'])
    ->name('admin.product.attributes.index')
    ->middleware(['auth:sanctum']);
Route::post('/dashboard-productos-atributos-guardar', [App\Http\Controllers\Dashboard\AttributeController::class, 'store'])
    ->name('admin.product.attributes.store')
    ->middleware(['auth:sanctum']);
Route::get('/dashboard-productos-atributos-editar/{attribute}/editar', [App\Http\Controllers\Dashboard\AttributeController::class, 'edit'])
    ->name('admin.product.attribute.edit')
    ->middleware(['auth:sanctum']);
Route::delete('/dashboard-productos-atributos-delete/{product}', [App\Http\Controllers\Dashboard\AttributeController::class, 'destroy'])
    ->name('admin.attributes.destroy')
    ->middleware(['auth:sanctum']);
//dashboard product;;
Route::get('/dashboard-productos', [App\Http\Controllers\Dashboard\ProductController::class, 'index'])
    ->name('admin.product.index')
    ->middleware(['auth:sanctum']);
Route::get('/dashboard-productos-crear', [App\Http\Controllers\Dashboard\ProductController::class, 'create'])
    ->name('admin.product.create')
    ->middleware(['auth:sanctum']);
Route::post('/dashboard-productos-guardar', [App\Http\Controllers\Dashboard\ProductController::class, 'store'])
    ->name('admin.product.store')
    ->middleware(['auth:sanctum']);
Route::post('/dashboard-productos-actualizar', [App\Http\Controllers\Dashboard\ProductController::class, 'update'])
    ->name('admin.product.update')
    ->middleware(['auth:sanctum']);
Route::get('/dashboard-productos-editar/{product}/editar', [App\Http\Controllers\Dashboard\ProductController::class, 'edit'])
    ->name('admin.product.edit')
    ->middleware(['auth:sanctum']);

Route::delete('/dashboard-productos-delete/{product}', [App\Http\Controllers\Dashboard\ProductController::class, 'destroy'])
    ->name('admin.product.destroy')
    ->middleware(['auth:sanctum']);

//Categorias
Route::get('/dashboard-productos-categorias', [App\Http\Controllers\Dashboard\CategoryController::class, 'index'])
    ->name('admin.product.categories.index')
    ->middleware(['auth:sanctum']);
Route::post('/dashboard-productos-categorias-guardar', [App\Http\Controllers\Dashboard\CategoryController::class, 'store'])
    ->name('admin.product.categories.store')
    ->middleware(['auth:sanctum']);

Route::delete('/dashboard-productos-categorias-delete/{category}', [App\Http\Controllers\Dashboard\CategoryController::class, 'destroy'])
    ->name('admin.category.destroy')
    ->middleware(['auth:sanctum']);
//Socialite
Route::get('auth/google', [SocialiteController::class, 'redirectToGoogle'])->name('auth.google');
Route::get('auth/google/callback', [SocialiteController::class, 'handleCallback']);
Route::get('auth/facebook', [SocialiteController::class, 'redirectToFacebook'])->name('auth.facebook');
Route::get('auth/facebook/callback', [SocialiteController::class, 'facebookSignin']);
