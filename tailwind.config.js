const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    content: [
        "./node_modules/flowbite/**/*.js"
    ],
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {

        maxHeight: {
            'aside-checkout': '26rem',
            'addresses': '500px',
            'products': '380px'

        },
        backgroundSize: {
            'auto': 'auto',
            'cover': 'cover',
            'contain': 'contain',
            '50%': '50%',
            '16': '4rem',
        },
        backgroundColor: theme => ({
            ...theme('colors'),
            'fb': '#3b5998 ',
            'logo': '#fff',
            'google': '#db4a39',
            'green': '#79B942',
            'coffe': '#613000',
            'white': '#ffffff',
            'quantity': '#F8CDBC',
            'background ': '#FDF4EB',
            'input-search-desktop': '#E3E8CA',
            'aside': '#EDDFD2',
            'orange': '#EB5B1F',
            'btn-orange': '#EB5B1F',
            'yellow': '#F6BB2E',
            'product': '#D3F8B3',

        }),
        textColor: theme => theme('colors'),
        textColor: {
            'white': '#FFF',
            'primary': '#613000',
            'danger': '#e3342f',
            'cafe': '#613000',
            'orange': '#EB5B1F',
            'green': '#79B942',
            'yellow': '#F6BB2E',
        },
        height: {
            sm: '8px',
            md: '16px',
            lg: '24px',
            clock: '2.5rem',
            xl: '48px',
            input: '61px',
            input_checkout: '50px',
            search: '65px',
            file: '85px',
            nav: '85px',
            img_search: '70px',
            logo: '100px',
            product_cart: '130px',
            progres_bar: '40px',
            avatar: '120px',
        },
        extend: {
            fontFamily: {
                sans: ['Ubuntu', ...defaultTheme.fontFamily.sans],
                // 'Ubuntu': ['Ubuntu', 'sans.serif'],
            },

            width: {
                'select': '15rem',
                'role': '18rem',
                'type-product': '19.5rem',
                'dropdown-products': '21.5rem',
                'dropdown': '22.9rem',
                'dropdown-search': '30rem',
            }

        },
        borderColor: theme => ({
            ...theme('colors'),
            DEFAULT: theme('colors.gray.300', 'currentColor'),
            'primary': '#79B942',
            'cafe': '#613000',
            'danger': '#e3342f',
        }),
        borderRadius: {
            'none': '0',
            'sm': '10px',
            'lg': '15px',
            'lg-2': '10px',
            'full': '50%',
        },
        colors: {
            background: '#FDF4EB',
            primary: '#79B942', //verde
            secundary: '#EB5B1F',//naranja
            tertiary: '#613000', //café
            contrast: '#F6BB2E', //amarillo
        }
    },
    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography'), require('flowbite/plugin')],

};
